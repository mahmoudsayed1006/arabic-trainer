import express from 'express';
import TrainingController from '../../controllers/training/training.controller';
import { requireAuth } from '../../services/passport';

const router = express.Router();

router.route('/')
    .post(
        requireAuth,
        TrainingController.validateBody(),
        TrainingController.create
    )
    .get(TrainingController.findAll);

router.route('/:trainingId')
    .put(
        requireAuth,
        TrainingController.validateBody(true),
        TrainingController.update
    )
    .delete(requireAuth,TrainingController.delete);

export default router;
