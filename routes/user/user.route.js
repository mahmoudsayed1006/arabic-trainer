import express from 'express';
import { requireSignIn, requireAuth } from '../../services/passport';
import UserController from '../../controllers/user/user.controller';
import { multerSaveTo } from '../../services/multer-service';

const router = express.Router();
router.post('/signin', requireSignIn, UserController.signIn);

router.route('/signup')
.post(
    multerSaveTo('users').single('profile'),
    UserController.validateUserCreateBody(),
    UserController.signUp
);
router.route('/trainersignup')
.post(
    multerSaveTo('trainers').single('profile'),
    UserController.validateTrainerCreateBody(),
    UserController.signUpTrainer
);
router.route('/centersignup')
.post(
    multerSaveTo('centers').single('profile'),
    UserController.validateCenterCreateBody(),
    UserController.signUpCenter
);
//get
router.route('/trainers')
.get(UserController.findAllTrainer);

router.route('/trainers/subscribe')
.get(UserController.findAllSubscribeTrainer);

router.route('/trainers/unaccepted')
.get(UserController.findAllUnacceptedTrainer);

router.route('/consultants')
.get(UserController.findAllConsultant);


router.route('/consultants/unaccepted')
.get(UserController.findAllUnacceptedconsalt);

router.route('/trainers/:trainerId')
.get(UserController.findTrainerById);


router.route('/trainers/notAccepted')
.get(UserController.filterTrainerByAccept);


router.route('/centers')
.get(UserController.findAllCenter);

router.route('/centers/subscribe')
.get(UserController.findAllCenterSubscribe);

router.route('/centers/unaccepted')
.get(UserController.findAllUnacceptedCenter);

router.route('/centers/:centerId')
.get(UserController.findCenterById);
//update
router.route('/update/Info')
.put(
    requireAuth,
    multerSaveTo('trainers').single('profile'),
    UserController.validateUpdatedInfo(),
    UserController.updateInfo
);

router.route('/update/Password')
.put(
    requireAuth,
    UserController.validateUpdatedPassword(),
    UserController.updatePassword
);
router.route('/update/Email')
.put(
    requireAuth,
    UserController.validateUpdatedEmail(),
    UserController.updateEmail
);

router.route('/update/trainerServiceCard')
.put(
    requireAuth,
    UserController.validateUpdatedServiceCard(),
    UserController.updateTrainerServiceCard
);

router.route('/update/trainerLinks')
.put(
    requireAuth,
    UserController.validateUpdatedSocialLinks(),
    UserController.updateTrainerSocialLinks
);

router.route('/:userId/accept')
.put(
    requireAuth,
   UserController.accept
);
router.route('/:userId/ignore')
.put(
    requireAuth,
    UserController.ignore
);

export default router;
