import express from 'express';
import SearchController from '../../controllers/search/search.controller';
const router = express.Router();

//contact search
router.route('/')
    .post(SearchController.validateSearch(),SearchController.search)


export default router;
