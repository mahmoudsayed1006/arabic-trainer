import express from 'express';
import adminController from '../../controllers/admin/admin.controller';

const router = express.Router();
router.route('/centers')
    .get(adminController.getLastCenter);

router.route('/trainers')
    .get(adminController.getLastTrainer);

router.route('/consultants')
    .get(adminController.getLastConsultant);

router.route('/count')
    .get(adminController.count);





export default router;
