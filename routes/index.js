import express from 'express';
import userRoute from './user/user.route';
import TrainingRoute from './training/training.route';
import SearchRoute from './search/search.route';
import PaypalRoute from './paypal/paypal.route';
import adminRoute from './admin/admin.route';
import notifRoute from './notif/notif.route';

const router = express.Router();

router.use('/', userRoute);
router.use('/training', TrainingRoute);
router.use('/search', SearchRoute);
router.use('/dashboard', adminRoute);
router.use('/notif', notifRoute);
router.use('/subscribe', PaypalRoute);

export default router;
