import express from 'express';

import PaypalController from '../../controllers/paypal/paypal.controller';
const router = express.Router();

router.route('/createPlane').get(
    PaypalController.createPlane
);

router.route('/createAgreement').get(
    PaypalController.createAgreement
);

router.route('/processAgreement').get(PaypalController.execute);

    


export default router;