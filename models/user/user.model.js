import mongoose, { Schema } from 'mongoose';
import autoIncrement from 'mongoose-auto-increment';
import { isImgUrl } from "../../helpers/CheckMethods";
import bcrypt from 'bcryptjs';
import isEmail from 'validator/lib/isEmail';
const userSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    name: {
        type: String,
        required: true,
        trim: true
    },
    nationality:{
        type: String,
        trim: true
    },
    country:{
        type: String,
        required: true,
        trim: true
    },
    city:{
        type: String,
        required: true,
        trim: true
    },
    email: {
        type: String,
        trim: true,
        required: true,
        validate: {
            validator: (email) => isEmail(email),
            message: 'Invalid Email Syntax'
        }

    },
    phone: {
        type: String,
        required: true,
        trim: true,
    },
    password: {
        type: String,
        required: true
    },
    type: {
        type: String,
        enum: ['CLIENT','ADMIN','CENTER','TRAINER'],
    },
    manager:{
        type: String,
        trim: true
    },
    YearFounded:{
        type:String,
    },
    profile: {
        type: String,
        validate: {
            validator: imgUrl => isImgUrl(imgUrl),
            message: 'img is invalid url'
        }
    },
    trainingField:{
        type: [String],
    },
    serviceCard:{
        type: [String]
    },
    gender:{
        type: String,
    },
    description:{
        type: String,
    },
    authorizationAuthority:{
        type: String,
    },
    courses:{
        type: [String],
        default:' '
    },
    facebook:{
        type: String,
        default:' ',
        trim:true
    },
    twitter:{
        type: String,
        default:' ',
        trim:true
    },
    snapchat:{
        type: String,
        default:' ',
        trim:true
    },
    youtube:{
        type: String,
        default:' ',
        trim:true
    },
    instgram:{
        type: String,
        default:' ',
        trim:true
    },
    linkedin:{
        type: String,
        default:' ',
        trim:true
    },
    telegram:{
        type: String,
        default:' ',
        trim:true
    },
    accept:{
        type:Boolean,
        default:false
    },
    subscribe:{
        type: String,
        enum: [
            '6 month trainer','6 month trainer special','12 month trainer','12 month trainer special',
            '6 month centers','6 month centers special','12 month centers','12 month trainer centers',
            'unsubscribe'
        ],
        default:'unsubscribe'
    },
    deleted: {
        type: Boolean,
        default: false
    }
}, { timestamps: true, discriminatorKey: 'kind' });

userSchema.pre('save', function (next) {
    const account = this;
    if (!account.isModified('password')) return next();

    const salt = bcrypt.genSaltSync();
    bcrypt.hash(account.password, salt).then(hash => {
        account.password = hash;
        next();
    }).catch(err => console.log(err));
});

userSchema.methods.isValidPassword = function (newPassword, callback) {
    let user = this;
    bcrypt.compare(newPassword, user.password, function (err, isMatch) {
        if (err)
            return callback(err);
        callback(null, isMatch);
    });
};

userSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret.password;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
userSchema.plugin(autoIncrement.plugin, { model: 'user', startAt: 1 });
export default mongoose.model('user', userSchema);