import mongoose, { Schema } from 'mongoose';
import autoIncrement from 'mongoose-auto-increment';
import { isImgUrl } from "../../helpers/CheckMethods";
import bcrypt from 'bcryptjs';
import isEmail from 'validator/lib/isEmail';
const trainingSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    description:{
        type: String,
        trim: true,
        required:true
    },
    title:{
        type:String,
        trim: true,
        required:true
    },
    date:{
        type:Date,
        default:Date.now
    },
    user: {
        type: String,
        required: true,
        ref:'user'
    },
    deleted: {
        type: Boolean,
        default: false
    }
}, { timestamps: true, discriminatorKey: 'kind' });

trainingSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret.password;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
trainingSchema.plugin(autoIncrement.plugin, { model: 'training', startAt: 1 });
export default mongoose.model('training', trainingSchema);