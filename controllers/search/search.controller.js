
import ApiResponse from "../../helpers/ApiResponse";
import { checkValidations } from "../shared/shared.controller";
import { checkExistThenGet, checkExist} from "../../helpers/CheckMethods";
import { body } from "express-validator/check";
import User from "../../models/user/user.model";

export default {
    validateSearch() {
        return [
            body('search').not().isEmpty().withMessage('search message required')
        ]
    }, 
    async search (req,res,next){
        try{
            const validatedBody = checkValidations(req);
            let page = +req.query.page || 1, limit = +req.query.limit || 20
            let query = {
                $and: [
                    { $or: [
                        
                        {name: { $regex: '.*' + validatedBody.search + '.*' }}, 
                        {country: { $regex: '.*' + validatedBody.search + '.*' }}, 
                        {city: { $regex: '.*' + validatedBody.search + '.*' }}, 
                        {trainingField: { $regex: '.*' + validatedBody.search + '.*' }}, 
                        {courses: { $regex: '.*' + validatedBody.search + '.*' }}, 
                        {serviceCard: { $regex: '.*' + validatedBody.search + '.*' }}, 
                      ] 
                    },
                    {deleted: false} 
                ]
            };
           console.log(query);
            let result = await User.find(query)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const resultCount = await User.count(query);
            const pageCount = Math.ceil(resultCount / limit);
            res.send(new ApiResponse(result, page, pageCount, limit, resultCount, req));
        }
        catch(err){
            next(err);
        }
    },
 
    
}