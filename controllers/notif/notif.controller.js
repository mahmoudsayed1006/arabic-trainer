import User from "../../models/user/user.model";
import { checkExistThenGet, checkExist} from "../../helpers/CheckMethods";
import Notif from "../../models/notif/notif.model";

export default {
    async find(req, res, next) {
        try {
            let user = req.user._id;
            await checkExist(req.user._id, User);
             let query =  { deleted: false, target:user }
            res.send(
                await Notif.find(query)
            );
        } catch (err) {
            next(err);
        }
    },
    async read(req, res, next) {
        try {
            let { notifId} = req.params;
            let notif = await checkExistThenGet(notifId, Notif);
            notif.read = true;
            await notif.save();
            res.send('notif read');
        } catch (error) {
            next(error);
        }
    },

    async unread(req, res, next) {
        try {
            let { notifId} = req.params;
            let notif = await checkExistThenGet(notifId, Notif);
            notif.read = false;
            await notif.save();
            res.send('notif unread');
        } catch (error) {
            next(error);
        }
    },
}