import { checkExistThenGet, checkExist } from '../../helpers/CheckMethods';
import { body } from 'express-validator/check';
import { checkValidations, handleImg ,handleImgs } from '../shared/shared.controller';
import { generateToken } from '../../utils/token';
import User from "../../models/user/user.model";
import ApiError from '../../helpers/ApiError';
import ApiResponse from "../../helpers/ApiResponse";
import bcrypt from 'bcryptjs';
import Notif from "../../models/notif/notif.model";
import { sendNotifiAndPushNotifi } from "../../services/notification-service";



export default {
    async signIn(req, res, next) {
        try{
            let user = req.user;
            user = await User.findById(user.id),
                res.status(200).send({
                    user,
                    token: generateToken(user.id)
                });
        } catch(err){
            next(err);
        }
    },

    validateUserCreateBody(isUpdate = false) {
        let validations = [
            body('name').not().isEmpty().withMessage('name is required'),
            body('nationality').not().isEmpty().withMessage('nationality is required'),
            body('phone').not().isEmpty().withMessage('phone is required')
                .custom(async (value, { req }) => {
                    let userQuery = { phone: value };
                    if (isUpdate && req.user.phone === value)
                        userQuery._id = { $ne: req.user._id };

                    if (await User.findOne(userQuery))
                        throw new Error(req.__('phone duplicated'));
                    else
                        return true;
                }),
            body('email').not().isEmpty().withMessage('email is required')
                .isEmail().withMessage('email syntax')
                .custom(async (value, { req }) => {
                    let userQuery = { email: value };
                    if (isUpdate && req.user.email === value)
                        userQuery._id = { $ne: req.user._id };

                    if (await User.findOne(userQuery))
                        throw new Error(req.__('email duplicated'));
                    else
                        return true;
                }),

                body('type').not().isEmpty().withMessage('type is required')
                .isIn(['CLIENT','ADMIN']).withMessage('wrong type'),
                body('country').not().isEmpty().withMessage('country is required'),
                body('city').not().isEmpty().withMessage('city is required'),
                body('profile').optional().custom(val => isImgUrl(val)).withMessage('profile should be a valid img'),
            
        ];
        if (!isUpdate) {
            validations.push([
                body('password').not().isEmpty().withMessage('password is required')
            ]);
        }
        return validations;
    },
    async signUp(req, res, next) {
        try {
            const validatedBody = checkValidations(req);
            let image = await handleImg(req);
            let createdUser = await User.create({
                ...validatedBody, profile:image
            });
            sendNotifiAndPushNotifi({
                targetUser: createdUser.id, 
                fromUser: 'المدرب العربى', 
                text: 'new notification',
                subject: 'رساله ترحيب',
                subjectType: ' '
            });
            let notif = {
                "description":'عزيزى الزائر حفظك الله ترحب بكم اداره مجموعه المدرب العربى الدوليه - لبنان نشكركم على انضمامكم لتطبيق المدرب العربى نامل منكم مراجعه بريدكم الالكترونى لتفعيل حسابكم والاستفاده من خدمات التطبيق اهلا وسهلا بكم الاداره'
            }
            await Notif.create({...notif,resource:req.user,target:createdUser.id});
            res.status(201).send({
                user: await User.findById(createdUser.id),
                token: generateToken(createdUser.id)
            });
            

        } catch (err) {
            next(err);
        }
    },
    validateTrainerCreateBody(isUpdate = false) {
        let validation = [
            body('name').not().isEmpty().withMessage('name is required'),
            body('nationality').not().isEmpty().withMessage('nationality is required'),
            body('phone').not().isEmpty().withMessage('phone is required')
                .custom(async (value, { req }) => {
                    let userQuery = { phone: value };
                    if (isUpdate && req.user.phone === value)
                        userQuery._id = { $ne: req.user._id };

                    if (await User.findOne(userQuery))
                        throw new Error(req.__('phone duplicated'));
                    else
                        return true;
                }),
            body('email').not().isEmpty().withMessage('email is required')
                .isEmail().withMessage('email syntax')
                .custom(async (value, { req }) => {
                    let userQuery = { email: value };
                    if (isUpdate && req.user.email === value)
                        userQuery._id = { $ne: req.user._id };

                    if (await User.findOne(userQuery))
                        throw new Error(req.__('email duplicated'));
                    else
                        return true;
                }),
            body('type').not().isEmpty().withMessage('type is required')
                .isIn(['CLIENT','ADMIN','CENTER','TRAINER']).withMessage('wrong type'),
            body('subscribe')
            .isIn([
                '6 month trainer','6 month trainer special','12 month trainer','12 month trainer special'
            ]).withMessage('wrong type'),
            body('gender').not().isEmpty().withMessage('gender is required'),
            body('country').not().isEmpty().withMessage('country is required'),
            body('city').not().isEmpty().withMessage('city is required'),
            body('description').not().isEmpty().withMessage('description is required')
            .isIn(['trainer','consultant']).withMessage('wrong type'),
            body('trainingField').not().isEmpty().withMessage('trainingField is required'),
            body('authorizationAuthority').not().isEmpty().withMessage('authorizationAuthority is required'),
            body('courses').not().isEmpty().withMessage('Courses is required'),
            body('serviceCard').not().isEmpty().withMessage('serviceCard is required'),
            body('profile').optional().custom(val => isImgUrl(val)).withMessage('profile should be a valid img'),

        ];
        if (!isUpdate) {
            validation.push([
                body('password').not().isEmpty().withMessage('password is required'),
               
            ])
        }
        return validation;
    },

    async signUpTrainer(req, res, next) {
        try {
            const validatedBody = checkValidations(req);
           
            let image = await handleImg(req);
            let createdTrainer = await User.create({
                ...validatedBody, profile:image
            });
            sendNotifiAndPushNotifi({
                targetUser: createdTrainer.id, 
                fromUser: 'المدرب العربى', 
                text: 'new notification',
                subject: 'رساله ترحيب',
                subjectType: ' '
            });
            let notif = {
                "description":"المكرم الزميل المدرب / المدربه - المستشار / المستشاره ترحب بكم ادراه مجموعهالمدرب العربى الدوليه - لبنان نشكركم على انضمامكم لتطبيق المدرب العربى نامل منكم مراجعه بريدكم الالكترونى لتفعيل حسابكم فالتطبيق والاستفاده من خدماته اهلا وسهلا بكم الاداره "
            }
            await Notif.create({...notif,resource:req.user,target:createdTrainer.id});
            res.status(201).send({
                trainer: await User.findById(createdTrainer.id),
                token: generateToken(createdTrainer.id)
            });
            

        } catch (err) {
            next(err);
        }
    },
    validateCenterCreateBody(isUpdate = false) {
        let validation = [
            body('name').not().isEmpty().withMessage('name is required'),
            body('phone').not().isEmpty().withMessage('phone is required')
                .custom(async (value, { req }) => {
                    let userQuery = { phone: value };
                    if (isUpdate && req.user.phone === value)
                        userQuery._id = { $ne: req.user._id };

                    if (await User.findOne(userQuery))
                        throw new Error(req.__('phone duplicated'));
                    else
                        return true;
                }),
            body('email').not().isEmpty().withMessage('email is required')
                .isEmail().withMessage('email syntax')
                .custom(async (value, { req }) => {
                    let userQuery = { email: value };
                    if (isUpdate && req.user.email === value)
                        userQuery._id = { $ne: req.user._id };

                    if (await User.findOne(userQuery))
                        throw new Error(req.__('email duplicated'));
                    else
                        return true;
                }),
            body('type').not().isEmpty().withMessage('type is required')
                .isIn(['CLIENT','ADMIN','CENTER','TRAINER']).withMessage('wrong type'),
            body('subscribe')
            .isIn([
                '6 month centers','6 month centers special','12 month centers','12 month trainer centers'
            ]).withMessage('wrong type'),
            body('country').not().isEmpty().withMessage('country is required'),
            body('city').not().isEmpty().withMessage('city is required'),
            body('manager').not().isEmpty().withMessage('manager is required'),
            body('YearFounded').not().isEmpty().withMessage('Year Founded is required'),
            body('trainingField').not().isEmpty().withMessage('trainingField is required'),
            body('serviceCard').not().isEmpty().withMessage('serviceCard is required'),
            body('profile').optional().custom(val => isImgUrl(val)).withMessage('profile should be a valid img'),
        ];
        if (!isUpdate) {
            validation.push([
                body('password').not().isEmpty().withMessage('password is required'),
                
            ])
        }
        return validation;
    },

    async signUpCenter(req, res, next) {
        try {
            const validatedBody = checkValidations(req);
            let image = await handleImg(req);
            let createdCenter = await User.create({
                ...validatedBody,profile:image
            });
            sendNotifiAndPushNotifi({
                targetUser: createdCenter.id, 
                fromUser: 'المدرب العربى', 
                text: 'new notification',
                subject: 'رساله ترحيب',
                subjectType: ' '
            });
            let notif = {
                "description":"المكرمين السادة معهد / مركز التدريب – الاستشارات حفظكم الله ترحب بكم ادراه مجموعهالمدرب العربى الدوليه - لبنان نشكركم على انضمامكم لتطبيق المدرب العربى نامل منكم مراجعه بريدكم الالكترونى لتفعيل حسابكم فالتطبيق والاستفاده من خدماته اهلا وسهلا بكم الاداره "
            }
            await Notif.create({...notif,resource:req.user,target:createdCenter.id});
            res.status(201).send({
                center: await User.findById(createdCenter.id),
                token: generateToken(createdCenter.id)
            });
            

        } catch (err) {
            next(err);
        }
    },
    async findAllTrainer(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let query = { deleted: false , description:"trainer",type:'TRAINER'};
            let trainers = await User.find(query)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const trainersCount = await User.count(query);
            const pageCount = Math.ceil(trainersCount / limit);

            res.send(new ApiResponse(trainers, page, pageCount, limit, trainersCount, req));
        } catch (err) {
            next(err);
        }
    },
    async findAllSubscribeTrainer(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let query = { deleted: false , type:"TRAINER", subscribe:{ $ne: 'unsubscribe' }};
            let trainers = await User.find(query)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const trainersCount = await User.count(query);
            const pageCount = Math.ceil(trainersCount / limit);

            res.send(new ApiResponse(trainers, page, pageCount, limit, trainersCount, req));
        } catch (err) {
            next(err);
        }
    },
    async findTrainerById(req, res, next) {
        try {
            let { trainerId } = req.params;
            await checkExist(trainerId, User, { deleted: false ,type:'TRAINER'});
            let trainer = await User.findById(trainerId);
            res.send(trainer);
        } catch (err) {
            next(err);
        }
    },
    async filterTrainerByAccept(req , res , next)
    {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let query = { deleted: false , accept:false,type:'TRAINER'};
            let trainers = await User.find(query)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const trainersCount = await User.count(query);
            const pageCount = Math.ceil(trainersCount / limit);

            res.send(new ApiResponse(trainers, page, pageCount, limit, trainersCount, req));
        } catch (err) {
            next(err);
        } 
    },
    async findAllConsultant(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let query = { deleted: false , description:'consultant',type:'TRAINER'};
            let consultants = await User.find(query)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const consultantsCount = await User.count(query);
            const pageCount = Math.ceil(consultantsCount / limit);

            res.send(new ApiResponse(consultants, page, pageCount, limit, consultantsCount, req));
        } catch (err) {
            next(err);
        }
    },
    async findAllCenter(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let query = { deleted: false, type:"CENTER"};
            let centers = await User.find(query)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const centersCount = await User.count(query);
            const pageCount = Math.ceil(centersCount / limit);

            res.send(new ApiResponse(centers, page, pageCount, limit, centersCount, req));
        } catch (err) {
            next(err);
        }
    },
    async findAllCenterSubscribe(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let query = { deleted: false , type:"CENTER",subscribe:{ $ne: 'unsubscribe' }};
            let trainers = await User.find(query)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const trainersCount = await User.count(query);
            const pageCount = Math.ceil(trainersCount / limit);

            res.send(new ApiResponse(trainers, page, pageCount, limit, trainersCount, req));
        } catch (err) {
            next(err);
        }
    },
    async findCenterById(req, res, next) {
        try {
            let { centerId } = req.params;
            await checkExist(centerId, User, { deleted: false ,type:'CENTER'});
            let center = await User.findById(centerId);
            res.send(center);
        } catch (err) {
            next(err);
        }
    },

    validateUpdatedInfo() {
        let validation = [
            body('name').not().isEmpty().withMessage('name is required'),
            body('phone').not().isEmpty().withMessage('phone is required'),
            body('country').not().isEmpty().withMessage('country is required'),
            body('city').not().isEmpty().withMessage('city is required'),
            body('description'),
            body('profile').optional().custom(val => isImgUrl(val)).withMessage('profile should be a valid img'),
           
        ];

        return validation;
    },
    async updateInfo(req, res, next) {
        try {
            const validatedBody = checkValidations(req);
            let image = await handleImg(req);
            let user = await checkExistThenGet(req.user._id, User);

            user.phone = validatedBody.phone;
            user.name = validatedBody.name;
            user.description = validatedBody.description;
            user.country = validatedBody.country;
            user.city = validatedBody.city;
            user.profile = image;
            await user.save();
           
            res.status(200).send({
                user: await User.findById(user.id)
            });

        } catch (error) {
            next(error);
        }
    },

    validateUpdatedPassword(isUpdate = false) {
        let validation = [
            body('newPassword').optional().not().isEmpty().withMessage('newPassword is required'),
            body('currentPassword').optional().not().isEmpty().withMessage('currentPassword is required')
           
        ];

        return validation;
    },
    async updatePassword(req, res, next) {
        try {
            let user = await checkExistThenGet(req.user._id, User);
            if (req.body.newPassword) {
                if (req.body.currentPassword) {
                    if (bcrypt.compareSync(req.body.currentPassword, user.password)) {
                        user.password = req.body.newPassword;
                    }
                    else {
                        res.status(400).send({
                            error: [
                                {
                                    location: 'body',
                                    param: 'currentPassword',
                                    msg: 'currentPassword is invalid'
                                }
                            ]
                        });
                    }
                }
            }
            await user.save();
            res.status(200).send({
                user: await User.findById(req.user._id)
            });

        } catch (error) {
            next(error);
        }
    },
    validateUpdatedEmail(isUpdate = false) {
        let validation = [
            body('email').not().isEmpty().withMessage('email is required')
            .isEmail().withMessage('email syntax')
            .custom(async (value, { req }) => {
                let userQuery = { email: value };
                if (isUpdate && req.user.email === value)
                    userQuery._id = { $ne: req.user._id };

                if (await User.findOne(userQuery))
                    throw new Error(req.__('email duplicated'));
                else
                    return true;
            }),
            body('password').optional().not().isEmpty().withMessage('newPassword is required'),

           
        ];

        return validation;
    },
    async updateEmail(req, res, next) {
        try {
            const validatedBody = checkValidations(req);
            let user =  await checkExistThenGet(req.user._id, User);
           
                if (req.body.password) {
                    if (bcrypt.compareSync(req.body.password, user.password)) {
                        user.email = validatedBody.email;
                        await user.save();
                        res.status(200).send({
                            user: await User.findById(user._id)
                        });
                    }
                    else {
                        res.status(400).send({
                            error: [
                                {
                                    location: 'body',
                                    param: 'password',
                                    msg: 'password is invalid'
                                }
                            ]
                        });
                    }
                }
            
           
        } catch (error) {
            next(error);
        }
    },

    validateUpdatedServiceCard() {
        let validation = [
            body('serviceCard').not().isEmpty().withMessage('serviceCard is required'),
           
        ];

        return validation;
    },
    async updateTrainerServiceCard(req, res, next) {
        try {
            const validatedBody = checkValidations(req);
            let user =  await checkExistThenGet(req.user._id, User);
            if (user.type == 'TRAINER' || user.type == 'CENTER'){
                user.serviceCard = validatedBody.serviceCard;
                await user.save();
                res.status(200).send({
                    user: await User.findById(user.id)
                });
            } else{
                return next(new ApiError(403, ('trainer or center auth')));
            }
              
            

        } catch (error) {
            next(error);
        }
    },

    validateUpdatedSocialLinks(isUpdate = false) {
        let validation = [
            body('facebook').not().withMessage('facebook must be link'),
            body('twitter').not().withMessage('twitter must be link'),
            body('youtube').not().withMessage('youtube must be link'),
            body('instgram').not().withMessage('instgram must be link'),
            body('snapchat').not().withMessage('snapchat must be link'),
            body('linkedin').not().withMessage('linkedin must be link'),
            body('telegram').not().withMessage('telegram must be link'),
        ];

        return validation;
    },
    async updateTrainerSocialLinks(req, res, next) {
        try {
            const validatedBody = checkValidations(req);
            let user =  await checkExistThenGet(req.user._id, User);
            if (user.type == 'TRAINER' || user.type == 'CENTER'){
                user.facebook = validatedBody.facebook;
                user.twitter = validatedBody.twitter;
                user.youtube = validatedBody.youtube;
                user.instgram = validatedBody.instgram;
                user.snapchat = validatedBody.snapchat;
                user.linkedin = validatedBody.linkedin;
                user.telegram = validatedBody.telegram;
            await user.save();
                res.status(200).send({
                    user: await User.findById(user.id)
                });
            } else{
                return next(new ApiError(403, ('trainer or center auth')));
            }   
        } catch (error) {
            next(error);
        }
    },

    async accept(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { userId} = req.params;
            let activeUser = await checkExistThenGet(userId,User);
            activeUser.accept = true;
            await activeUser.save();
            sendNotifiAndPushNotifi({
                targetUser: userId, 
                fromUser: req.user, 
                text: 'new notification',
                subject: activeUser.id,
                subjectType: 'arabic trainer accept your register'
            });
            res.send(activeUser);
            
        } catch (error) {
            next(error);
        }
    },

    async ignore(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { userId} = req.params;
            let activeUser = await checkExistThenGet(userId,User);
            activeUser.accept = false;
            await activeUser.save();
            
            res.send(activeUser);
        } catch (error) {
            next(error);
        }
    },
    async findAllUnacceptedTrainer(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let query = {
                $and: [
                    {description:"trainer"},
                    {accept:false},
                    {deleted: false} 
                ]
            };
            let trainers = await User.find(query)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const trainersCount = await User.count(query);
            const pageCount = Math.ceil(trainersCount / limit);

            res.send(new ApiResponse(trainers, page, pageCount, limit, trainersCount, req));
        } catch (err) {
            next(err);
        }
    },
    async findAllUnacceptedconsalt(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let query = {
                $and: [
                    {description:"consultant"},
                    {type:'TRAINER'},
                    {accept:false},
                    {deleted: false} 
                ]
            };
            let trainers = await User.find(query)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const trainersCount = await User.count(query);
            const pageCount = Math.ceil(trainersCount / limit);

            res.send(new ApiResponse(trainers, page, pageCount, limit, trainersCount, req));
        } catch (err) {
            next(err);
        }
    },
    async findAllUnacceptedCenter(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let query = {
                $and: [
                    {type:"CENTER"},
                    {accept:false},
                    {deleted: false} 
                ]
            };
            let trainers = await User.find(query)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const trainersCount = await User.count(query);
            const pageCount = Math.ceil(trainersCount / limit);

            res.send(new ApiResponse(trainers, page, pageCount, limit, trainersCount, req));
        } catch (err) {
            next(err);
        }
    },


};
