import ApiResponse from "../../helpers/ApiResponse";
import Training from "../../models/training/training.model";
import User from "../../models/user/user.model";
import { checkExist, checkExistThenGet} from "../../helpers/CheckMethods";
import { checkValidations } from "../shared/shared.controller";
import { body } from "express-validator/check";
import ApiError from '../../helpers/ApiError';

const populateQuery = [
    { path: 'user', model: 'user' }
];

export default {
    async findAll(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let query = { deleted: false };
            let training = await Training.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const trainingCount = await Training.count(query);
            const pageCount = Math.ceil(trainingCount / limit);

            res.send(new ApiResponse(training, page, pageCount, limit, trainingCount, req));
        } catch (err) {
            next(err);
        }
    },

    validateBody(isUpdate = false) {
        let validations = [
            body('title').not().isEmpty().withMessage('title is required'),
            body('description').not().isEmpty().withMessage('description is required'),
            body('date').not().isEmpty().withMessage('date is required'),
        ];

        return validations;
    },

    async create(req, res, next) {
        try {
            const validatedBody = checkValidations(req);
            let user =  await checkExistThenGet(req.user._id, User);
            if (user.type == 'CLIENT'){
                let createdTraining = await Training.create({ ...validatedBody ,user:req.user._id });
                res.status(200).send(createdTraining);
            } else{
                return next(new ApiError(403, ('client auth')));
            } 
        } catch (err) {
            next(err);
        }
    },
    async update(req, res, next) {
        try {
            let user = req.user._id;
            let { trainingId } = req.params;
            await checkExist(trainingId, Training, { deleted: false , user:user });

            const validatedBody = checkValidations(req);
            let updatedTraining = await Training.findByIdAndUpdate(trainingId, {
                ...validatedBody,
            }, { new: true });
            res.status(200).send(updatedTraining);
        }
        catch (err) {
            next(err);
        }
    },
    async delete(req, res, next) {
        try {
            let { trainingId } = req.params;
            let user = req.user._id;
            let training = await checkExistThenGet(trainingId, Training, { deleted:false, user:user});
            training.deleted = true;

            await training.save();
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },

    
};