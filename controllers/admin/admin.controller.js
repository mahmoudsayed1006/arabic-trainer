import ApiError from "../../helpers/ApiError";
import User from "../../models/user/user.model";

export default {
    async getLastCenter(req, res, next) {
        try {
            let query = {
                $and: [
                    {deleted: false},
                    {type:'CENTER'}
                ]
            };
         
            let lastUser = await User.find(query)
                .sort({ createdAt: -1 })
                .limit(10);

            res.send(lastUser);
        } catch (error) {
            next(error);
        }
    },
    async getLastTrainer(req, res, next) {
        try {
           
            let query = {deleted: false,type:'TRAINER',description:'trainer'};
         
            let lastUser = await User.find(query)
                .sort({ createdAt: -1 })
                .limit(10);

            res.send(lastUser);
        } catch (error) {
            next(error);
        }
    },
    async getLastConsultant(req, res, next) {
        try {
            let user = req.user;
           
            let query = {deleted: false,type:'TRAINER',description:'consultant'};
         
            let lastUser = await User.find(query)
                .sort({ createdAt: -1 })
                .limit(10);

            res.send(lastUser);
        } catch (error) {
            next(error);
        }
    },
   
    async count(req,res, next) {
        try {
            const centerCount = await User.count({deleted:false,type:'CENTER'});
            const consultantCount = await User.count({deleted:false,type:'TRAINER',description:'consultant'});
            const trainerCount = await User.count({deleted:false,type:'TRAINER',description:'trainer'});
            
            res.status(200).send({
                center:centerCount,
                consultant:consultantCount,
                trainer:trainerCount,
            });
        } catch (err) {
            next(err);
        }
        
    },
    
}