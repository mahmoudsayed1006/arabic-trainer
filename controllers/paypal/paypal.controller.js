var paypal = require('paypal-rest-sdk');

paypal.configure({
    'mode': 'sandbox', //sandbox or live
    'client_id': 'ARVy_EX-haDYZ7ACZudaJ4GH4Dkx_z9rfp64vb1HnQoNwrm5bhBS9KdBd2A5hBPHhFiYcvu6H8j8le5V',
    'client_secret': 'EBJt0oR73HqfBhkzoShKUAhSXUI4b1n6F73BWyc10NB7n3vl2mQ83xzy85k4uEQ1g-flOMoOJFnl6MdY'
  });

export default {
    async createPlane(req,res,next){
      console.log(req.headers['user-agent'])
        try{
            var billingPlanAttribs = {
                "name": "Food of the World Club Membership: Standard",
                "description": "Monthly plan .",
                "type": "fixed",
                "payment_definitions": [
                    {
                        "name": "6 month trainer",
                        "type": "REGULAR",
                        "frequency_interval": "1",
                        "frequency": "6 MONTH",
                        "cycles": "11",
                        "amount": {
                            "currency": "USD",
                            "value": "24"
                        }
                    },
                    {
                        "name": "12 month trainer",
                        "type": "REGULAR",
                        "frequency_interval": "1",
                        "frequency": "12 MONTH",
                        "cycles": "11",
                        "amount": {
                            "currency": "USD",
                            "value": "48"
                        }
                    },
                    {
                        "name": "6 month trainer special",
                        "type": "REGULAR",
                        "frequency_interval": "1",
                        "frequency": "6 MONTH",
                        "cycles": "11",
                        "amount": {
                            "currency": "USD",
                            "value": "38"
                        }
                    },
                    {
                        "name": "12 month trainer special",
                        "type": "REGULAR",
                        "frequency_interval": "1",
                        "frequency": "6 MONTH",
                        "cycles": "11",
                        "amount": {
                            "currency": "USD",
                            "value": "60"
                        }
                    },
                    //centers
                    {
                        "name": "6 month centers",
                        "type": "REGULAR",
                        "frequency_interval": "1",
                        "frequency": "6 MONTH",
                        "cycles": "11",
                        "amount": {
                            "currency": "USD",
                            "value": "36"
                        }
                    },
                    {
                        "name": "12 month centers",
                        "type": "REGULAR",
                        "frequency_interval": "1",
                        "frequency": "12 MONTH",
                        "cycles": "11",
                        "amount": {
                            "currency": "USD",
                            "value": "72"
                        }
                    },
                    {
                        "name": "6 month centers special",
                        "type": "REGULAR",
                        "frequency_interval": "1",
                        "frequency": "6 MONTH",
                        "cycles": "11",
                        "amount": {
                            "currency": "USD",
                            "value": "56"
                        }
                    },
                    {
                        "name": "12 month centers special",
                        "type": "REGULAR",
                        "frequency_interval": "1",
                        "frequency": "6 MONTH",
                        "cycles": "11",
                        "amount": {
                            "currency": "USD",
                            "value": "92"
                        }
                    },
                ],
                "merchant_preferences": {
                    "setup_fee": {
                        "currency": "USD",
                        "value": "1"
                    },
                    "cancel_url": "http://localhost:3000/cancel",
                    "return_url": "http://localhost:3000/processagreement",
                    "max_fail_attempts": "0",
                    "auto_bill_amount": "YES",
                    "initial_fail_amount_action": "CONTINUE"
                }
            };
            
            var billingPlanUpdateAttributes = [{
                "op": "replace",
                "path": "/",
                "value": {
                    "state": "ACTIVE"
                }
            }];
            
            paypal.billingPlan.create(billingPlanAttribs, function (error, billingPlan){
                if (error){
                    console.error(JSON.stringify(error));
                    throw error;
                } else {
                    // Activate the plan by changing status to Active
                    paypal.billingPlan.update(billingPlan.id, billingPlanUpdateAttributes, 
                        function(error, response){
                        if (error) {
                            console.log(error);
                            console.error(JSON.stringify(error));
                            throw error;
                        } else {
                            console.log('Billing plan created under ID: ' + billingPlan.id);
                        }
                    });
                }
            }); 
        } catch(err){
            next(err)
        }
    },
    async createAgreement(req,res,next){
        try{
            var billingPlan = req.query.plan;
            var isoDate = new Date();
            isoDate.setSeconds(isoDate.getSeconds() + 4);
            isoDate.toISOString().slice(0, 19) + 'Z';
            var billingAgreementAttributes = {
                "name": "Standard Membership",
                "description": "arabic trainer",
                "start_date": isoDate,
                "plan": {
                    "id": billingPlan
                },
                "payer": {
                    "payment_method": "paypal"
                },
                "shipping_address": {
                    "line1": "W 34th St",
                    "city": "New York",
                    "state": "NY",
                    "postal_code": "10001",
                    "country_code": "US"
                }
            };
              // Use activated billing plan to create agreement
                await paypal.billingAgreement.create(billingAgreementAttributes, function (
                error, billingAgreement){
                if (error) {
                    console.error(error);
                    throw error;
                } else {
                    //capture HATEOAS links
                    var links = {};
                    billingAgreement.links.forEach(function(linkObj){
                        links[linkObj.rel] = {
                            'href': linkObj.href,
                            'method': linkObj.method
                        };
                    })

                    //if redirect url present, redirect user
                    if (links.hasOwnProperty('approval_url')){
                        res.redirect(links['approval_url'].href);
                    } else {
                        console.error('no redirect URI present');
                    }
                }
                });
        } catch(err){
            next(err)
        }
    },
    async execute(req,res,next){
        try{
            var token = req.query.token;
            paypal.billingAgreement.execute(token, {}, function (error, 
                billingAgreement) {
                if (error) {
                    console.error(error);
                    throw error;
                } else {
                    console.log(JSON.stringify(billingAgreement));
                    res.send('Billing Agreement Created Successfully');
                }
            });
        } catch(err){
            next(err)
        }
    }
}